// ----------------------------
//  OpenSCAD settings
// ----------------------------
$fn=16;

// ----------------------------
//  User settings
// ----------------------------
// need at least 8m from top plate's bottom for switch
// and an extra 3-4mm for the arduino
top_plate_thickness = 3;
plate_rounding = 7;
bottom_height = 8;  // includes bottom_thickness
bottom_thickness = 2;
bottom_wall_thickness = 2;
extra_wall_height = 1; // makes a step in the top plate, too

inclination_enable = true;
inclination_angle = atan(-1/130); // 1mm drop over 130mm run
inclination_offset = -(16.5 + plate_rounding); // stick-out below x-axis (thumb plate)

arduino_base_dims = [18,33,1.5];
arduino_with_components = [arduino_base_dims.x,arduino_base_dims.y,3];
usb_micro_b_port_dims = [8,6,2.5];
usb_micro_b_stick_out = 1.5;

switch_width = 14;
switch_height_with_pegs = 8; // just the part below the top of the plate
switch_height_just_base = 5; // ditto
switch_depth = switch_width;
switch_mount_thickness = 1.5;
switch_mount_rounding = 0.3;

screw_head_diameter = 6; // [mm]
screw_head_height = 2; // [mm]
screw_diameter = 3.1; // [mm]
screw_height = 20; // [mm]
screw_hole_padding = 1.5; // [mm]

kEps = .001;
kBottomTopSep = 20; // visual separation

{{
// intersection() { translate([37,-500,-500]) cube([1000,1000,1000]); union() {
// intersection() { translate([35,-500,-500]) cube([1000,1000,1000]); union() {

// intersection() {
//     union() {
//         keys_for_plate(key_plate_def, positive=true);
//         thumb_plate_transform() keys_for_plate(thumb_plate_def, positive=true);
//     };
//     translate([0,0,top_plate_thickness/2]) cube([1000,1000,top_plate_thickness], center=true);
// };


top_part();
color("green") translate([0,0,-kBottomTopSep])
    bottom_part(draft=inclination_enable);
// switch_stencil();

module top_part() {
    difference() {
    plate();
    keys_for_plate(key_plate_def, positive=false);
    thumb_plate_transform() keys_for_plate(thumb_plate_def, positive=false);
    bottom_part(draft=false);
    };
}

module bottom_part(draft) {
    module inclination_cube() {
        if (draft && inclination_angle != 0)
        translate([0,inclination_offset,bottom_height]) rotate([inclination_angle,0,0]) translate([0,0,1000/2])
            cube([1000, 1000, 1000], center=true);
    }
    // walls
    translate([0,0,-bottom_height])
    difference() {
        linear_extrude(bottom_height+extra_wall_height) difference() {
            projection() plate(bottom_mode=true);
            offset(-bottom_wall_thickness) projection() plate(bottom_mode=true);
        };
        translate([0,0,extra_wall_height]) inclination_cube();
        ardunino_transform() arduino();
        translate([121,73.5-3,bottom_height+top_plate_thickness-switch_height_just_base-2.6])
            cube([15,16,3]);
    }

    translate([0,0,-bottom_height])
    difference() {
        union() {
            // bottom
            plate(bottom_mode=true);
            // posts, etc.
            intersection() { // make sure all positive features are in-bounds
                linear_extrude(100) offset(-kEps) projection() plate(bottom_mode=true);
                union() {
                    thumb_plate_transform() keys_for_plate(thumb_plate_def, positive=true, bottom_mode=true);
                    keys_for_plate(key_plate_def, positive=true, bottom_mode=true);
                };
            };
        };
        // screw holes, etc.
        thumb_plate_transform() keys_for_plate(thumb_plate_def, positive=false, bottom_mode=true);
        keys_for_plate(key_plate_def, positive=false, bottom_mode=true);
        ardunino_transform() arduino();
        inclination_cube();
    };
}

}};

module ardunino_transform() {
    translate([138.65,33.75,bottom_height+top_plate_thickness-switch_height_just_base])
    translate([-1.5,+6,0])
    rotate([0,180,129])
    // translate([0,-3.4,0])
    children();
}
module thumb_plate_transform() {
    translate([104,-0.5,0]) rotate(-30) children();
}
module plate(bottom_mode=false) {
    plate_for(key_plate_def, bottom_mode=bottom_mode);
    thumb_plate_transform() plate_for(thumb_plate_def, bottom_mode=bottom_mode);
}

module plate_for(def, edge="chamfer", edge_break_frac=0.25, bottom_mode=false) {
    plate_thickness = bottom_mode ? bottom_thickness : top_plate_thickness;
    plane_thickness = plate_thickness*(1-edge_break_frac);
    oversizing = bottom_mode ? kEps*1 : 0;
    translate([0,0,plate_thickness/2])
    minkowski() {
        intersection() {
            hull() keys_for_plate(def, positive=true, just_extents=true);
            cube([1000,1000,plane_thickness], center=true);
        };
        resize([plate_rounding*2+oversizing, plate_rounding*2+oversizing, plate_thickness-plane_thickness]) {
            if (bottom_mode || edge == "mitre") {
                cylinder(r=1,h=1,center=true);
            } else if (edge == "fillet") {
                translate([0,0,-.5]) intersection() { // hemisphere
                    sphere(r=1);
                    translate([0,0,1]) cube([2,2,2],center=true);
                };
            } else if (edge == "chamfer") {
                cylinder(
                    r1=plate_rounding,
                    r2=plate_rounding-(plate_thickness-plane_thickness),
                    h=plate_thickness-plane_thickness,
                    center=true
                );
            }
        };
    }
}

module arduino(extend_usb_back=true) { // origin: y-aligned, bottom of pcb side with the usb port; port sticks out into -y
    translate([-arduino_base_dims.x/2,0,0]) cube(arduino_base_dims);
    translate([-usb_micro_b_port_dims.x/2, -usb_micro_b_stick_out, arduino_base_dims.z])
        cube(usb_micro_b_port_dims + [0,extend_usb_back ? arduino_base_dims.y/2 : 0, 0]);
}

module switch_stencil(txt="..", just_extents=false) {
    bottom_width = switch_width + .4; // space for the tabs
    bottom_dims = [bottom_width, bottom_width, 15];

    // slight vertical undersize for a snug fit, and slight horizontal oversize,
    // as (my) gateron browns down work well if compressed in that direction.
    top_dims = [switch_width + .1, switch_width - .1, 15];

    // Bottom
    if (!just_extents) {
        translate([0,0,top_plate_thickness - switch_mount_thickness - bottom_dims.z/2])
            rrect(bottom_dims, switch_mount_rounding, center=true);
    }

    // Top
    rrect(top_dims, switch_mount_rounding, center=true);
}

module screw() {
    screw_head_extended_height = screw_head_height + 5; // ensure clearance above screw
    translate([0,0,-screw_head_height]) { // origin is middle of top of head
        translate([0,0,screw_head_extended_height/2])
            cylinder(h=screw_head_extended_height, d=screw_head_diameter, center=true);
        translate([0,0,-screw_height*0.4/2 + kEps*4])
            cylinder(h=screw_height*0.4 + kEps*8, d=screw_diameter*1.05, center=true);
        translate([0,0,-screw_height/2 + kEps])
            cylinder(h=screw_height + 2*kEps, d=screw_diameter, center=true);
    };
}

module support_post(h, d, fillet_radius=0) { // origin: center of bottom
    translate([0,0,h/2]) cylinder(h=h, d=d, center=true);
    if (fillet_radius > 0) cylinder_fillet(d/2 - kEps, fillet_radius);
}

// Defaults: // left/bottom
u1 = [5,5];
_HIDDEN_ = [u1, 0, "", 1];

key_plate_def = [
    // clock-wise, from top left
    [[16.5,94,true,[0,-2]],[113,94.5,true,[0,-2]],[116,29,false],[37,1],[16.5,-3,false]], // screws
    [[55,61],[93,57.5],[73,23.5],[34.5,35.5]], // posts
    [[125,1]], // [132,10] extra points
    /* [
            margins[left, bottom],
            tilt (clockwise, in deg),
            label/tag,
            hidden (1), visible (default:0)
        ]
    */
    [
        /*control keys */
        [u1, 0, "CTRL"],
        // [u1 + [0,9.5], 0, "CTRL"],
        [u1, 0, "SHFT"],
        [u1, 0, "`~"],
        [u1, 0, "TAB"],
        // _HIDDEN_,
        [u1, 0, "ESC"],
    ],
    [
        /* Mostly alpha-num #1 */
        [u1, 0, "ALT"],         // Y:3.375
        [u1, 0, "Z"],
        [u1, 0, "A"],
        [u1, 0, "Q"],
        [u1, 0, "1!"],
    ],
    [
        /* Mostly alpha-num #2 */
        [u1 + [0,5], 0, "-_"], //  Y:3.125
        [u1, 0, "X"],
        [u1, 0, "S"],
        [u1, 0, "W"],
        [u1, 0, "2@"],
    ],
    [
        /* Mostly alpha-num #3 */
        [u1 + [0,7], 0, "=+"], // Y:3
        [u1, 0, "C"],
        [u1, 0, "D"],
        [u1, 0, "E"],
        [u1, 0, "3#"],
    ],
    [
        /* Alpha-num #4 */
        _HIDDEN_,
        [u1 + [0,5], 0, "V"],  // Y:3.125
        [u1, 0, "F"],
        [u1, 0, "R"],
        [u1, 0, "4$"],
    ],
    [
        /* Alpha-num #5 */
        _HIDDEN_,
        [u1 + [0,3], 0, "B"],  // Y:3.25
        [u1, 0, "G"],
        [u1, 0, "T"],
        [u1, 0, "5%"],
    ],
    [
        /* Special layer keys */
        _HIDDEN_,
        _HIDDEN_,
        [u1 + [0,0], 0, "[\u2193]"], // Layer down, Down arrow
        [u1 + [0,0], 0, "[\u2191]"], // Layer up, Up arrow
        [u1 + [0,0], 0, "a"], // Layer up, Up arrow
    ],
    [
        /* Space key */
        [u1 + [-50, 2], 15, "SPC"], 
    ],
];


thumb_plate_def = [
    // clock-wise, from top left
    [[35,18,true,[-2,0]]], // screws
    [], // posts
    [[-15,0],[-15,35]], // extra points
    [
        [u1, 0, "[L]"],
        [u1 + [0,3], 0, "HOME"]
    ],
        [
        [u1, 0, "BCK"],
        [u1 + [0,3], 0, "END"] 
    ],
 ];

// Indices
margins = 0;
tilt = 1;
tag = 2;
margin_left = 0;
margin_top = 1;
invisible = 3;

function findOffsetX(def, row, max_col, col=0) = 
    def[col][row][margins][0] + (col < max_col ? findOffsetX(def, row, max_col, col + 1) : 0);

function findOffsetY(def, col, max_row, row=0) = 
    def[col][row][margins][1] + (row < max_row ? findOffsetY(def, col, max_row, row + 1) : 0);

// Build plate for key plate definition.
module keys_for_plate(full_def, positive, just_extents=false, bottom_mode=false) {
    plate_thickness = bottom_mode ? bottom_thickness : top_plate_thickness;
    def = [ for (i=[3:len(full_def)-1]) full_def[i] ];
    if (!bottom_mode && (!positive || just_extents)) for (col=[0:len(def)-1]) {
        for (row=[0:len(def[col])-1]) {
            if (!def[col][row][invisible]) {
                translate([
                    2 + findOffsetX(def, row, col) + (col * switch_width),
                    2 + findOffsetY(def, col, row) + (row * switch_depth),
                    0]
                ) {
                    rotate(-def[col][row][tilt]){
                        switch_stencil(def[col][row][tag], just_extents=just_extents);
                    }
                }
            }
        }
    }

    screws = full_def[0];
    for (s = screws) {
        enable = len(s) <= 2 ? true : s[2];
        extents_offset = len(s) <= 3 ? [0,0] : s[3];
        if (enable) translate([s.x,s.y,plate_thickness]) {
            if (just_extents) {
                translate([extents_offset.x, extents_offset.y,0]) cylinder(r=kEps,h=20,center=true);
            } else if (bottom_mode) {
                if (positive) {
                    translate([0,0,-kEps]) support_post(
                        h=bottom_height-bottom_thickness,
                        d=screw_diameter+screw_hole_padding*2,
                        fillet_radius=3
                    );
                } else {
                    translate([0,0,bottom_height+top_plate_thickness-screw_head_height]) screw();
                }
            } else {
                screw();
            }
        }
    }

    posts = full_def[1];
    for (s = posts) {
        enable = len(s) <= 2 ? true : s[2];
        if (enable) translate([s.x,s.y,plate_thickness]) {
            if (just_extents) {
                // nothing -- not a top feature, so don't want to affect the outline
            } else if (bottom_mode) {
                if (positive)
                    translate([0,0,-kEps]) support_post(
                        h=bottom_height-bottom_thickness,
                        d=4,
                        fillet_radius = 3
                    );
            } else {
                // nothing -- not a top feature
            }
        }
    }

    extra_ponits = full_def[2];
    for (s = extra_ponits) {
        translate([s.x,s.y,plate_thickness]) {
            if (just_extents) cylinder(r=kEps,h=20,center=true);
            // else, nothing -- only wanted for determining the extents
        }
    }
}

module rrect(dims,r,center=false) {
    w = dims.x; d = dims.y; h = dims.z;
    off = center ? [-w/2,-d/2,-h/2] : [0,0,0];
    translate(off) hull() {
        translate([r,r,0]) cylinder(r=r,h=h);
        translate([w-r,r,0]) cylinder(r=r,h=h);
        translate([r,d-r,0]) cylinder(r=r,h=h);
        translate([w-r,d-r,0]) cylinder(r=r,h=h);
    };
}

module cylinder_fillet(inner_radius, fillet_radius) {
    difference() {
        cylinder(h=fillet_radius, r=inner_radius + fillet_radius);
        rotate_extrude() {
            translate([inner_radius + fillet_radius, fillet_radius,0]) circle(r=fillet_radius);
        };
    }
}