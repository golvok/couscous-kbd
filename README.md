# CousCous Lasercut

Sources & tooling:
* Design base: [thingiverse.com](https://www.thingiverse.com/thing:2704567)
* Layout design: [Keyboard layout editor](http://www.keyboard-layout-editor.com/#/)
* Layout to CAD: [Plate & case builder](http://builder.swillkb.com/) (use raw_keyboard_layout.txt)
* Case design: [Inkscape](https://inkscape.org/)

# CousCous 3D Printed 
Parametric OpenSCAD keyboard design

Inspired by MattDB's redox keyboard design on [thingiverse.com](https://www.thingiverse.com/thing:2704567) which is in turn inspired by the [Ergodox keyboard design](https://deskthority.net/wiki/ErgoDox).

I wanted to add some changes to the redox keyboard, but can't make a 3D design if my life depended on it, so I decided to build a parametric design in [OpenSCAD](http://www.openscad.org/) because I _can_ program. A bit.

First aim is to copy MattDB's design in parametric form.

Cherry MX Switch stencil:
![switch_stencil](samples/switch_stencil.png)

## Key plate definition
(TODO: describe, when done, maybe just use the keyboard-layout-editor format?)

## Samples

### Left hand key plate definition
```javascript
key_plate_def = [
    [
        /*control keys */
        [u1, 0, "CTRL"],
        [u1, 0, "SHFT"],
        [u1, 0, "`~"],
        [u1, 0, "TAB"],
        [u1, 0, "ESC"],
    ],
    [
        /* Mostly alpha-num #1 */
        [u1, 0, "ALT"],
        [u1, 0, "Z"],
        [u1, 0, "A"],
        [u1, 0, "Q"],
        [u1, 0, "1!"],
    ],
    [
        /* Mostly alpha-num #2 */
        [u1 + [0,5], 0, "-_"],
        [u1, 0, "X"],
        [u1, 0, "S"],
        [u1, 0, "W"],
        [u1, 0, "2@"],
    ],
    [
        /* Mostly alpha-num #3 */
        [u1 + [0,7], 0, "=+"],
        [u1, 0, "C"],
        [u1, 0, "D"],
        [u1, 0, "E"],
        [u1, 0, "3#"],
    ],
    [
        /* Alpha-num #4 */
        _HIDDEN_,
        [u1 + [0,5], 0, "V"],
        [u1, 0, "F"],
        [u1, 0, "R"],
        [u1, 0, "4$"],
    ],
    [
        /* Alpha-num #5 */
        _HIDDEN_,
        [u1 + [0,3], 0, "B"],
        [u1, 0, "G"],
        [u1, 0, "T"],
        [u1, 0, "5%"],
    ],
    [
        /* Special layer keys */
        [u1 + [-30, 4], 12, "SPC"],
        _HIDDEN_,
        _HIDDEN_,
        [u1 + [4,-8], 0, "[\u2193]"], // Layer down, Down arrow
        [u1 + [4,0], 0, "[\u2191]"], // Layer up, Up arrow
    ]
];
```


![Left-hand key plate](samples/left_hand_key_plate.png)

## Issues
* Would like to use the UTF-8 'vol. up', 'vol. down' and 'mute' glyphs, have yet to find a font supporting these and regular characters too :-/.
* Enough todo's left inside code.
* Does not (yet) seem to render well.
